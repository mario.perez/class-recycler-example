package edu.uca.ejemplorecyclerview.ui.interfaces;

/**
 * Created by marioperezt on 10/17/17.
 */

public interface ItemClickListener<T> {
    void OnClick(T item);
}
