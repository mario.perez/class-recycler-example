package edu.uca.ejemplorecyclerview.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.uca.ejemplorecyclerview.R;
import edu.uca.ejemplorecyclerview.data.StudentsData;
import edu.uca.ejemplorecyclerview.models.Student;
import edu.uca.ejemplorecyclerview.ui.adapters.StudentAdapter;
import edu.uca.ejemplorecyclerview.ui.interfaces.ItemClickListener;


public class MainActivity extends AppCompatActivity {

    private List<Student> mItemList;
    private RecyclerView rvData;
    private StudentAdapter mStudentAdapter;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = this;
        mItemList = new ArrayList<>();
        rvData = (RecyclerView) findViewById(R.id.rv_data);
        mStudentAdapter = new StudentAdapter(mItemList, new ItemClickListener<Student>() {
            @Override
            public void OnClick(Student item) {

                Intent intent =
                        new Intent(mActivity, DetailStudentActivity.class);

                intent.putExtra("StudentName"
                        , item.getmName());

                intent.putExtra("StudentGrade",String.valueOf(item.getmGrade()));

                startActivity(intent);

                //Toast.makeText(mActivity,item.getmName(),Toast.LENGTH_SHORT).show();
            }
        });
        rvData.setAdapter(mStudentAdapter);
        rvData.setHasFixedSize(true);
        LinearLayoutManager mLLCatalogData = new LinearLayoutManager(this);
        mLLCatalogData.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(mLLCatalogData);
        mItemList.clear();
        mItemList.addAll(StudentsData.getData());
        mStudentAdapter.notifyDataSetChanged();

    }
}
