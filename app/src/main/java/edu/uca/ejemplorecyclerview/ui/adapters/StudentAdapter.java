package edu.uca.ejemplorecyclerview.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import edu.uca.ejemplorecyclerview.R;
import edu.uca.ejemplorecyclerview.models.Student;
import edu.uca.ejemplorecyclerview.ui.interfaces.ItemClickListener;
import edu.uca.ejemplorecyclerview.ui.viewholders.GradesViewHolder;

/**
 * Created by marioperezt on 10/11/17.
 */

public class StudentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Student> mListItems;
    private ItemClickListener<Student> mClickListener;


    public StudentAdapter(List<Student> items, ItemClickListener<Student> clickListener){
        mListItems = items;
        mClickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grades,parent,false);
        return new GradesViewHolder(v, mClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GradesViewHolder viewHolder = (GradesViewHolder) holder;
        viewHolder.setItem(mListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }
}
